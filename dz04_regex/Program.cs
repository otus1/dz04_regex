﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace dz04_regex
{
    class Program
    {
        static void Main(string[] args)
        {
            string html = getResponse(@"https://otus.ru/learning/38431/?utm_source=email&utm_medium=email&utm_campaign=otus&utm_term=hw_rejected&token=d5676a0a0b56fdd9b1365415d302acb17cec2668&relogin=True#/");
            //string HRefPattern = @"href\s*=\s*(?:[""'](?<1>[^""']+)[""']|(?<1>[^\s>]+))";
            string HRefPattern= @"(?<1>(http|https)://([^""'\s>]+))";
            DumpHrefs(html, HRefPattern);
            Console.ReadKey();
        }
        static string getResponse(string uri)
        {
            StringBuilder sb = new StringBuilder();
            byte[] buf = new byte[8192];
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream resStream = response.GetResponseStream();
            int count = 0;
            do
            {
                count = resStream.Read(buf, 0, buf.Length);
                if (count != 0)
                {
                    sb.Append(Encoding.Default.GetString(buf, 0, count));
                }
            }
            while (count > 0);
            return sb.ToString();
        }
        static void DumpHrefs(string html, string pattern)
        {
            Match m;
            
            try
            {
                m = Regex.Match(html, pattern,
                                RegexOptions.IgnoreCase | RegexOptions.Compiled,
                                TimeSpan.FromSeconds(1)
                    );
                while (m.Success)
                {
                    Console.WriteLine(m.Groups[1]);
                    m = m.NextMatch();
                }
            }

            catch (RegexMatchTimeoutException)
            {
                Console.WriteLine("The matching operation time out.");
            }
        }
    }
}
